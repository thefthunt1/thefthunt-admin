import axios from "axios";

const onRequestSuccess = (config: any) => {
  const token =
    localStorage.getItem("authenticationToken") ||
    sessionStorage.getItem("authenticationToken");
  if (token) {
    if (!config.headers) {
      config.headers = {};
    }
    config.headers.Authorization = `Bearer ${token}`;
  }
  config.timeout = process.env.VUE_APP_REQUEST_TIMEOUT;
  config.url = `${process.env.VUE_APP_SERVERURL}${config.url}`;
  return config;
};

const setupAxiosInterceptors = (onUnauthenticated: any, onServerError: any) => {
  const onResponseError = (err: any) => {
    const status = err.status || err.response.status;
    if (status === 403 || status === 401) {
      return onUnauthenticated(err);
    }
    if (status >= 500) {
      return onServerError(err);
    }
    return Promise.reject(err);
  };

  if (axios.interceptors) {
    axios.interceptors.request.use(onRequestSuccess);
    axios.interceptors.response.use((res) => res, onResponseError);
  }
};

axios.interceptors.request.use(
  function (config) {
    document.body.classList.add("loading-indicator");
    return config;
  },
  function (error) {
    document.body.classList.remove("loading-indicator");
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    document.body.classList.remove("loading-indicator");

    return response;
  },
  function (error) {
    document.body.classList.remove("loading-indicator");
    return Promise.reject(error);
  }
);

export { onRequestSuccess, setupAxiosInterceptors };
