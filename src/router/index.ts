import Login from "../components/login/Login.vue";
import UserList from "../components/sidebar/users/UserList.vue";
import Items from "../components/sidebar/items/Items.vue";
import Item from "../components/sidebar/items/ItemsEdit.vue";
import UserInfo from "../pages/userinfo/UserInfo.vue";
import Category from "../components/sidebar/category/Category.vue";
import CategoryEdit from "../components/sidebar/category/CategoryEdit.vue";
import Navigation from "../components/navigation/Navigation.vue";
import { createRouter, createWebHistory } from "vue-router";
import UserItem from "../components/sidebar/users/UserItem.vue";
import ItemsEdit from "@/components/sidebar/items/ItemsEdit.vue";
import NotFound from "../components/NotFound/NotFound.vue";
const routes = [
  {
    name: "Login",
    component: Login,
    path: "/",
    meta: { requiresUnAuth: true },
  },
  {
    name: "Navigation",
    component: Navigation,
    path: "/navigation",
    meta: { requiresAuth: true },
    children: [
      {
        name: "navigation-users",
        component: UserList,
        path: "users",
        // props: true,
        meta: { requiresAuth: true },
      },
      {
        name: "user-info",
        component: UserInfo,
        path: "user/info/:id?",
        meta: { requiresAuth: true },
        // props: true,
      },
      {
        name: "items",
        component: Items,
        path: "items",
        meta: { requiresAuth: true },
      },
      {
        name: "item",
        component: ItemsEdit,
        path: "/user/:userId/items/item/:id?",
        meta: { requiresAuth: true },
      },
      {
        name: "navigation-category",
        component: Category,
        path: "categories",
        meta: { requiresAuth: true },
        children: [
          {
            name: "navigation-category-edit",
            component: CategoryEdit,
            path: "category/:id?",
            meta: { requiresAuth: true },
          },
        ],
      },
      {
        path: "users/add",
        name: "user-add",
        component: UserItem,
        meta: { requiresAuth: true },
      },
      {
        path: "users/edit/:id",
        name: "user-edit",
        component: UserItem,
        meta: { requiresAuth: true },
      },
    ],
  },
  {
    name: "NotFound",
    component: NotFound,
    path: "/:notFound(.*)",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  linkActiveClass: "active",
});

export default router;
