export default {
    authenticate(state) {
        state.logon = true;
    },
    authenticated(state, identity) {
        state.userIdentity = identity;
        state.authenticated = true;
        state.logon = false;
    },
    logout(state) {
        state.userIdentity = null;
        state.authenticated = false;
        state.logon = false;
    },
    setNewItems(state, itemNumber){
        state.newItems = itemNumber
    }
}
