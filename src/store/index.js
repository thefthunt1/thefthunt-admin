import { createStore } from "vuex";
import mutations from './mutations.js'
import getters from './getters.js'

const store = createStore({
    state() {
        return {
            logon: false,
            userIdentity: null,
            authenticated: false,
            newItems: null
        }
    },
    mutations,
    getters
});

export default store;
