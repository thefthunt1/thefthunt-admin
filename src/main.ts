import { createApp } from "vue";

import App from "./App.vue";
import router from "./router";
import Toast, { PluginOptions, POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";
import "./assets/tailwind.css";
import AlertService from "./services/alert/alert.services";
import LoginComponent from "./components/login/login.component";
import NavigationComponent from "./components/navigation/navigation.component";
import UserListComponent from "./components/sidebar/users/userList.component";
import UserItemComponent from "./components/sidebar/users/userItem.component";
import DropZone from "./components/DropZone/DropZone.vue";
import FilePreview from "./components/DropZone/FilePreview.vue";
import { setupAxiosInterceptors } from "./axios-interceptor";
import Select2 from "vue3-select2-component";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import "vue-select/dist/vue-select.css";
import Tree from "vue3-treeview";
import store from "./store/index.js";
import AccountService from "./services/account/account.service";
import NotFound from "./components/NotFound/NotFound.vue";
import CategoryEdit from "./components/sidebar/category/CategoryEdit.vue";

let vSelect = require("vue-select");

import {
  Dialog,
  DialogPanel,
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  TransitionChild,
  TransitionRoot,
} from "@headlessui/vue";
import {
  Bars3BottomLeftIcon,
  BellIcon,
  XMarkIcon,
} from "@heroicons/vue/24/outline";
import {
  MagnifyingGlassIcon,
  ChevronUpDownIcon,
  ChevronUpIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "@heroicons/vue/20/solid";
import SearchComponent from "@/components/search/SearchComponent.vue";
import Pagination from "@/components/pagination/Pagination.vue";
const app = createApp(App);
const alertService = new AlertService();
const accountService = new AccountService(store);
app.provide("alertService", alertService);
app.provide("accountService", accountService);
app.component("LoginComponent", LoginComponent);
app.component("NavigationComponent", NavigationComponent);
app.component("UserListComponent", UserListComponent);
app.component("UserItemComponent", UserItemComponent);
app.component("v-select", vSelect);

app.component("TransitionChild", TransitionChild);
app.component("SearchComponent", SearchComponent);
app.component("MenuButton", MenuButton);
app.component("MenuItem", MenuItem);
app.component("MenuItems", MenuItems);
app.component("Menu", Menu);
app.component("Dialog", Dialog);
app.component("DropZone", DropZone);
app.component("FilePreview", FilePreview);
app.component("DialogPanel", DialogPanel);
app.component("TransitionRoot", TransitionRoot);
app.component("Tree", Tree);

app.component("XMarkIcon", XMarkIcon);
app.component("Bars3BottomLeftIcon", Bars3BottomLeftIcon);
app.component("MagnifyingGlassIcon", MagnifyingGlassIcon);
app.component("ChevronDownIcon", ChevronDownIcon);
app.component("ChevronUpIcon", ChevronUpIcon);
app.component("ChevronUpDownIcon", ChevronUpDownIcon);

app.component("BellIcon", BellIcon);
app.component("ChevronLeftIcon", ChevronLeftIcon);
app.component("ChevronRightIcon", ChevronRightIcon);
app.component("Pagination", Pagination);
app.component("Select2", Select2);
app.component("CategoryEdit", CategoryEdit);
const options: PluginOptions = {
  position: POSITION.TOP_RIGHT,
  timeout: 3000,
};
app.component("NotFound", NotFound);
app.use(Toast, options);
app.use(router);
app.use(VueSweetalert2);

router.beforeEach(async (to: any, from, next) => {
  if (to.meta.requiresAuth && !store.getters.authenticated) {
    next("/");
  } else if (to.meta.requiresUnAuth && store.getters.authenticated) {
    next("/navigation/users");
  } else {
    next();
  }
});

setupAxiosInterceptors(
  (error: any) => {
    const url = error.response?.config?.url;
    const status = error.status || error.response.status;
    if (status === 401) {
      accountService.logout();
      router.push("/");
    }
    return Promise.reject(error);
  },
  (error: any) => {
    console.log("Server error!");
    return Promise.reject(error);
  }
);

app.mount("#app");
