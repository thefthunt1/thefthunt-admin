export class NodeData {
  public text: string;
  public children: string[];
}

export class Node {
  [key: string]: NodeData;
}
