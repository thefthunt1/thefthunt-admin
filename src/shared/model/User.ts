import { Country } from "./Country";

export class User {
  public id: number;
  public name: string;
  public email: string;
  public password: string;
  public address: string;
  public postcode: string;
  public city: string;
  public country: Country;
  public created_at: string;
  public updated_at: string;
  public profile_image: string;
}
