export class Category {
  public id: number;
  public name: string;
  public parent_id: number;
  public children: Category[] | null;
}
