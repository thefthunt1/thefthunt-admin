import { RouterLink, RouterView } from "vue-router";
import { Inject, Options, Vue } from "vue-property-decorator";
import AccountService from "./services/account/account.service";
import store from "@/store";
import axios from "axios";

function getNewItems() {
  axios
    .get("/initial")
    .then((res) => {
      store.commit("setNewItems", res.data.data.new_items_badge);
    })
    .catch((error) => console.log(error));
}

@Options({
  // Specify `components` option.
  // See Vue.js docs for all available options:
  // https://vuejs.org/v2/api/#Options-Data
  components: {
    RouterLink,
    RouterView,
  },
})
export default class AppComponent extends Vue {
  @Inject()
  private accountService!: AccountService;

  created() {
    this.authenticated;
  }
  getNewItems() {
    console.log("");
  }
  public get authenticated(): boolean {
    if (this.accountService.authenticated) {
      this.accountService
        .checkCredentials()
        .then((authResult) => {
          getNewItems();
          return authResult;
        })
        .catch((_) => {
          return false;
        });
    }
    return false;
  }
}
