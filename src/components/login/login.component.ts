import { Inject, Vue } from "vue-property-decorator";
import type AlertService from "../../services/alert/alert.services";
import type AccountService from "@/services/account/account.service";

export default class LoginComponent extends Vue {
  @Inject()
  readonly accountService!: AccountService;
  @Inject()
  readonly alertService!: AlertService;
  public enteredEmail: string | undefined = "";
  public enteredPassword: string | undefined = "";
  submitForm() {
    this.accountService
      .login(this.enteredEmail!, this.enteredPassword!)
      .then(() => {
        this.$router.push({ name: "navigation-users" });
      })
      .catch((error) => {
        if (error.response.data) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public get authenticated(): boolean {
    return this.accountService.authenticated;
  }
}
