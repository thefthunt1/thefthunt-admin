import { Vue, Watch, Prop } from "vue-property-decorator";

export default class SearchComponent extends Vue {
  public searchForText: string = "";
  public inputText: string = "";

  @Prop()
  public search: (text: string) => void;
  public placeholder: string;

  @Watch("inputText")
  public onChange() {
    this.search(this.inputText);
  }
}
