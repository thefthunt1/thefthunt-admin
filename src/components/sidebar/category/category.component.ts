import { Inject, Vue } from "vue-property-decorator";
import type AlertService from "../../../services/alert/alert.services";
import axios from "axios";
import { Category } from "@/shared/model/Category";

export default class CategoryComponent extends Vue {
  /*
  Example of structure 
  public nodes = {
    id1: {
      text: "text1",
      children: ["id11", "id12"],
    },
    id11: {
      text: "text11",
    },
    id12: {
      text: "text12",
    },
    id2: {
      text: "text2",
    },
  };

  public config = {
    roots: ["id1", "id2"],
    editable: true,
    dragAndDrop: true,
  };
  */

  public nodes = [];
  public config = {
    roots: [],
    editable: true,
    dragAndDrop: true,
  };

  public nodeName: string = "";
  public isEditing: boolean = false;
  public nodeId: string = "";

  @Inject()
  readonly alertService!: AlertService;

  created() {
    this.initialize();
  }

  private initialize() {
    axios
      .get(`/categories`)
      .then((res) => {
        this.makeStructure(res.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  private makeStructure(data: Category[]) {
    data.forEach((element: Category) => {
      const id = element.id.toString();
      const nodeId = +this.$route.params.id;
      if (nodeId && this.nodes[nodeId] !== undefined) {
        this.edit(nodeId);
      }
      this.nodes[id] = {
        text: element.name,
        children: [],
        state: {
          opened: true,
        },
      };

      if (!element.parent_id) {
        this.config.roots.push(id);
      } else {
        this.nodes[element.parent_id].children.push(element.id.toString());
      }

      if (element.children) {
        this.makeStructure(element.children);
      }
    });
  }

  public onBlur(updatedNode: any) {
    this.isEditing = !!this.$route.params.id;
    if (
      updatedNode.type === "blur" &&
      updatedNode.target.className === "node-input"
    ) {
      this.nodeName = updatedNode.target.value;
      const nodeId =
        updatedNode.target.parentElement.previousElementSibling.innerHTML;
      // if (this.nodes[nodeId].text === changedName) return;

      let updatedData: object = {
        name: this.nodeName,
      };
      this.editCategory(nodeId, updatedData);
    }
  }

  public onDrop(updatedNode: any) {
    let updatedData: object = {
      parent_id: updatedNode.target.node,
    };

    this.editCategory(updatedNode.dragged.node.id, updatedData);
  }

  public async addCategory() {
    const name = "New category";

    axios
      .post(`/categories`, { name: name })
      .then((res) => {
        const id = res.data.data.category_id;

        this.nodes[id] = {
          text: name,
          children: [],
        };
        this.config.roots.push(id);
        this.alertService.showSuccess(res.data.message);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  private editCategory(id: string, updateData: {}) {
    axios
      .patch(`/categories/${id}`, updateData)
      .then((res) => {
        this.alertService.showSuccess(res.data.message);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public edit(nodeId: number) {
    if (this.nodes[nodeId] !== undefined) {
      this.$router.push({
        name: "navigation-category-edit",
        params: { id: nodeId },
      });
      this.isEditing = true;
    }
  }
  cancelEdit() {
    this.isEditing = false;
    this.nodes = [];
    this.config = {
      roots: [],
      editable: true,
      dragAndDrop: true,
    };

    this.initialize();
  }
}
