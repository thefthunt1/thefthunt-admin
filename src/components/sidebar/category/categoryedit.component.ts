import { Vue, Inject } from "vue-property-decorator";
import type AlertService from "../../../services/alert/alert.services";
import axios from "axios";

export default class CategoryEditComponent extends Vue {
  @Inject()
  readonly alertService!: AlertService;

  public categoryImagePreview: string = "";
  public categoryImage: string = "";
  public editedName: string = "";
  public category: any = {};
  mounted() {
    const id = +this.$route.params.id;
    if (id) {
      axios
        .get(`/categories/${id}/edit`)
        .then((res) => {
          this.showExistingCategoryImage(res.data.data.image);
          this.editedName = res.data.data.name;
          this.category = res.data.data;
        })
        .catch((error) => {
          if (error.response.data !== undefined) {
            this.alertService.showError(error.response.data.message);
          } else {
            this.alertService.showError(error.message);
          }
        });
    }
  }

  public goToCategpriesNavigation() {
    this.$emit("cancelEdit", false);
    this.$router.push({ name: "navigation-category" });
  }

  public showExistingCategorymage(categoryImage) {
    if (categoryImage) {
      this.categoryImagePreview = categoryImage;
    }
  }

  public onInputChange(e) {
    if (e.target.files[0]) {
      const file = e.target.files[0];
      this.categoryImagePreview = URL.createObjectURL(file);
      this.categoryImage = file;
    } else {
      this.categoryImage = null;
    }
  }

  public UpdateCategory() {
    let id = +this.$route.params.id;
    let data = new FormData();
    if (this.categoryImage) {
      data.append("image", this.categoryImage);
    }
    data.append("_method", "PATCH");
    data.append("name", this.editedName);

    axios
      .post(`/categories/${id}`, data)
      .then((res) => {
        this.alertService.showSuccess(res.data.message);
        this.$emit("cancelEdit", false);
        this.$router.push({ name: "navigation-category" });
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  private showExistingCategoryImage(categoryImage) {
    if (categoryImage) {
      this.categoryImagePreview = categoryImage;
    }
  }
}
