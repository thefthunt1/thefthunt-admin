import { Inject, Vue, Options, Watch } from "vue-property-decorator";
import axios from "axios";
import type AlertService from "../../../services/alert/alert.services";
import SearchComponent from "@/components/search/SearchComponent.vue";
import { User } from "@/shared/model/User";

@Options({
  components: {
    SearchComponent,
  },
})
export default class UserListComponent extends Vue {
  @Inject()
  readonly alertService!: AlertService;
  public users: User[] = [];
  public totalPages: any = 0;
  public itemsPerPage: any = 0;
  public currentPage: any = 1;

  public page: number = 1;
  public searchTerm: string = "";

  public predicate: any = "";
  public reverse: boolean = true;
  public sortQuery: string = "";

  declare $swal;

  @Watch("page")
  @Watch("searchTerm")
  @Watch("sortQuery")
  loadData() {
    let queryBuilder = `page=${this.page}`;

    if (this.searchTerm) {
      queryBuilder += `&filter[searchTerm]=${this.searchTerm}`;
    }

    if (this.sortQuery) {
      queryBuilder += `${this.sortQuery}`;
    }

    axios
      .get(`/users?${queryBuilder}`)
      .then((res) => {
        this.totalPages = res.data.meta.total;
        this.currentPage = this.page;
        this.itemsPerPage = res.data.meta.per_page;

        return (this.users = [...res.data.data]);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  sort(predicate) {
    this.reverse = !this.reverse;

    const direction = this.reverse ? "-" : "";
    this.sortQuery = `&sort=${direction + predicate}`;
  }

  created() {
    this.loadData();
  }

  public addUser() {
    this.$router.push({ name: "user-add" });
  }

  public editUser(user: User) {
    this.$router.push({ name: "user-edit", params: { id: user.id } });
  }

  public deleteUser(userId: number) {
    axios
      .delete(`/users/${userId}`)
      .then((res) => {
        this.loadData();
        this.alertService.showSuccess(res.data.message);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public setPage(page: number): void {
    this.page = page;
  }

  public setSearchTerm(text: string) {
    this.searchTerm = text;
  }

  public showUser(user: User) {
    this.$router.push({ name: "user-info", params: { id: user.id } });
  }

  public showDeleteAlert(userId: number, name: string) {
    this.$swal({
      title: "Delete user",
      position: "top",
      showCancelButton: true,
      cancelButtonText: "Cancel",
      confirmButtonText: "Delete",
      text: `Are you sure you want to delite user: ${name}?`,
    }).then((res) => {
      if (res.isConfirmed) {
        this.deleteUser(userId);
      } else if (res.dismiss === this.$swal.DismissReason.cancel) {
      }
    });
  }
}
