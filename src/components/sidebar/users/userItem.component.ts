import { Inject, Vue } from "vue-property-decorator";
import axios from "axios";
import type AlertService from "../../../services/alert/alert.services";
import { User } from "@/shared/model/User";
import { Country } from "@/shared/model/Country";

export default class UserItemComponent extends Vue {
  @Inject()
  readonly alertService!: AlertService;
  public countries: Country[] = [];
  public isEdit: boolean = false;
  public buttonText = "Create";

  public user: User = new User();
  public originalUser: User = new User();
  public profileImagePreview: string = "";

  created() {
    if (this.$route.params.id) {
      this.isEdit = true;
      this.buttonText = "Update";
      this.updateData();
    } else {
      this.getCountries();
    }
  }
  public getCountries() {
    axios
      .get(`/countries`)
      .then((res) => {
        return (this.countries = [...res.data.data]);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public updateData() {
    axios
      .get(`/countries`)
      .then((res) => {
        axios
          .get(`/users/${this.$route.params.id}/edit`)
          .then((res) => {
            this.user = { ...res.data.data.user };
            this.showExistingProfileImage(this.user.profile_image);
            this.originalUser = { ...res.data.data.user };
            return;
          })
          .catch((error) => {
            if (error.response.data !== undefined) {
              this.alertService.showError(error.response.data.message);
            } else {
              this.alertService.showError(error.message);
            }
          });

        return (this.countries = [...res.data.data]);
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public goToUserNavigation() {
    this.isEdit = false;
    this.$router.push({ name: "navigation-users" });
  }

  public createOrUpdate() {
    const updatedUser: object = {};
    if (this.isEdit) {
      Object.keys(this.user).some((field) => {
        if (this.user[field] !== this.originalUser[field]) {
          if (field === "country") {
            updatedUser["country_id"] = this.user[field].id;
          } else {
            updatedUser[field] = this.user[field];
          }
        }
      });
      if (Object.keys(updatedUser).length !== 0) {
        let data = new FormData();
        for (var key in updatedUser) {
          if (key === "profile_image") {
            if (updatedUser[key]) {
              data.append(key, updatedUser[key]);
            }
          } else {
            data.append(key, updatedUser[key]);
          }
        }
        data.append("_method", "PATCH");

        axios
          .post(`/users/${this.$route.params.id}`, data)
          .then((res) => {
            this.alertService.showSuccess(res.data.message);
            this.goToUserNavigation();
          })
          .catch((error) => {
            if (error.response.data !== undefined) {
              this.alertService.showError(error.response.data.message);
            } else {
              this.alertService.showError(error.message);
            }
          });
      } else {
        this.alertService.showError("No changes were made.");
      }
    } else {
      this.createUser();
    }
  }

  public createUser() {
    var userObject = {
      name: this.user.name,
      email: this.user.email,
      password: this.user.password,
      address: this.user.address,
      postcode: this.user.postcode,
      city: this.user.city,
      country_id: this.user.country.id,
      profile_image: this.user.profile_image,
    };

    let data = new FormData();
    for (var key in userObject) {
      if (key === "profile_image") {
        if (userObject[key]) {
          data.append(key, userObject[key]);
        }
      } else {
        data.append(key, userObject[key]);
      }
    }

    axios
      .post("/users", data)
      .then((res) => {
        this.alertService.showSuccess(res.data.message);
        this.goToUserNavigation();
      })
      .catch((error) => {
        if (error.response.data !== undefined) {
          this.alertService.showError(error.response.data.message);
        } else {
          this.alertService.showError(error.message);
        }
      });
  }

  public showExistingProfileImage(profileImage) {
    if (profileImage) {
      this.profileImagePreview = profileImage;
    }
  }

  public onInputChange(e) {
    if (e.target.files[0]) {
      const file = e.target.files[0];
      this.profileImagePreview = URL.createObjectURL(file);
      this.user.profile_image = file;
    } else {
      this.user.profile_image = null;
    }
  }
}
