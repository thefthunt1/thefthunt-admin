import { Inject, Options, Vue } from "vue-property-decorator";
import { ref } from "vue";
import {
  FolderIcon,
  UsersIcon,
  AdjustmentsVerticalIcon,
} from "@heroicons/vue/24/outline";
import axios from "axios";
import UserListComponent from "../sidebar/users/UserList.vue";
import ItemsComponent from "../sidebar/items/Items.vue";
import CategoryComponent from "../sidebar/category/Category.vue";
import AccountService from "@/services/account/account.service";
import store from "@/store";
@Options({
  components: {
    UserListComponent,
    ItemsComponent,
    CategoryComponent,
    UsersIcon,
    FolderIcon,
    AdjustmentsVerticalIcon,
  },
})
export default class NavigationComponent extends Vue {
  @Inject()
  readonly accountService!: AccountService;
  public sidebarOpen: any | undefined = ref(false);
  public searchForText: string = "";
  public profileImage: string;

  public navigation = [
    {
      name: "Users",
      href: "/navigation/users",
      icon: UsersIcon,
      current: true,
      componentName: UserListComponent,
    },
    {
      name: "Category",
      href: "/navigation/categories",
      icon: AdjustmentsVerticalIcon,
      current: false,
      componentName: CategoryComponent,
    },
    {
      name: "Items",
      href: "/navigation/items",
      icon: FolderIcon,
      current: false,
      componentName: ItemsComponent,
    },
  ];
  public userNavigation = [
    { name: "Your Profile", href: "#" },
    { name: "Settings", href: "#" },

    { name: "Sign out", href: "#" },
  ];

  created() {
    this.profileImage = localStorage.getItem("profileImage");
  }

  currentPage(_, componentName: string) {
    this.navigation.forEach((x) => (x.current = false));

    this.navigation.forEach((x) => {
      if (x.name === componentName) {
        x.current = true;
      }
    });
  }

  userNavigationClicked(userNavigationName) {
    if (userNavigationName === "Your Profile") {
      var loggedAccount = store.getters.account;
      axios
        .get(`/users?filter[email]=${loggedAccount}`)
        .then((res) => {
          this.$router.push({
            name: "user-info",
            params: { id: res.data.data[0].id },
          });
        })
        .catch((error) => {
          console.log("error");
          console.log(error);
        });
    }

    if (userNavigationName === "Sign out") {
      this.accountService.logout();
      this.$router.push("/");
    }
  }
  getNewItems() {
    return store.getters.newItems;
  }
}
