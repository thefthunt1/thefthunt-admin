import axios from "axios";
import type { Store } from "vuex";
export default class AccountService {
  constructor(private store: Store<any>) {}

  public login(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      var login = { email, password, device: process.env.VUE_APP_DEVICE };
      axios
        .post("/login", login)
        .then((res) => {
          localStorage.setItem("authenticationToken", res.data.data.token);
          localStorage.setItem(
            "authenticated",
            JSON.stringify(res.data.data.email)
          );
          localStorage.setItem("profileImage", res.data.data.profile_image);
          this.store.commit("authenticated", res.data.data.email);

          resolve(res);
        })
        .catch((res) => {
          reject(res);
        });
    });
  }

  public logout() {
    localStorage.removeItem("authenticationToken");
    localStorage.removeItem("authenticated");
    localStorage.removeItem("profileImage");
    sessionStorage.removeItem("authenticationToken");
    sessionStorage.removeItem("authenticationToken");
    sessionStorage.removeItem("profileImage");
    this.store.commit("logout");
  }

  public get authenticated(): boolean {
    const authenticated = localStorage.getItem("authenticated");
    if (authenticated != null) {
      this.store.commit("authenticated", JSON.parse(authenticated));
    }
    return this.store.getters.authenticated;
  }

  public checkCredentials(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios
        .get("/me")
        .then((res) => {
          const userName = localStorage.getItem("authenticated");
          const providedToken = localStorage.getItem("authenticationToken");

          if (
            res.data.data.name === userName &&
            res.data.data.token === providedToken
          ) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch((res) => {
          this.logout();
          reject(false);
        });
    });
  }
}
