import { TYPE, useToast } from "vue-toastification";
import type { ToastOptions } from "vue-toastification/dist/types/types";

export default class AlertService {
    public showError(message: string, params?: (ToastOptions & {
        type?: TYPE.ERROR | undefined;
    }) | undefined) {
        const toast = useToast();        
        toast.error(message,params);
    }

    public showSuccess(message: string, params?: (ToastOptions & {
        type?: TYPE.SUCCESS | undefined;
    }) | undefined) {
        const toast = useToast();
        toast.success(message,params);
  }
}